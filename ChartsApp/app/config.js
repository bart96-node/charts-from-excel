'use strict';

class Config {
  constructor() {
    this.mode = 'development'; // ["development", "production"];

    this.window = {
      main: {
        width:700,
        height:500,
        backgroundColor:'#47906b',
        title: 'Приступим к работе?',
      },
    };

  }
}



module.exports = Config;
