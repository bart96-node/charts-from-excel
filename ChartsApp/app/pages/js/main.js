const {ipcRenderer} = require('electron');
const XLSX = require('xlsx');


class Main {
  constructor() {
    document.addEventListener("DOMContentLoaded", event => {
      this.answer = document.getElementById('answer');
      this.yes = document.getElementById('yes');
      this.dropArea = document.getElementById('dropArea');
      this.counter = document.getElementById('counter');
      this.datalist = document.getElementById('datalist');
      this.sheetList = document.getElementById('sheetList');
      this.myChart = document.getElementById('myChart');

      this.book = '';
      this.bookSheets = [];

      ipcRenderer.on('clear', () => this._ipcRenderer());

      this.answer_click();
      this.dropArea_drop();
      this.sheetList_chage();
    });
  }


  _ipcRenderer() {
    let action = document.querySelector('body > [style^="opacity: 1;"]') || document.querySelector('body > :not([style])');
    this._fade(action, this.dropArea, this._standat);
  }


  _readFile(files) {
    this.book = XLSX.readFile(files[0].path);
    this.bookSheets = this.book.SheetNames;

    if (this.bookSheets.length > 1) {
      this.bookSheets.forEach((value, index) => this.datalist.innerHTML += `<option value="${++index}. ${value}">`);
      this._fade(this.dropArea, this.counter);
    }
    else this._fadeOut(this.dropArea);
  }

  _fadeOut(DOM, callback) {
    let opacity = 50;
    let interval = setInterval(() => {
      if (opacity > 0) DOM.style.opacity = --opacity / 50;
      else {
        DOM.style.display = 'none';
        clearInterval(interval);
        if (callback && typeof callback == 'function') callback();
      }
    }, 10);
  }

  _fadeIn(DOM, callback) {
    let opacity = 0;
    DOM.style.display = '';

    let interval = setInterval(() => {
      if (opacity < 50) DOM.style.opacity = ++opacity / 50;
      else {
        clearInterval(interval);
        if (callback && typeof callback == 'function') callback();
      }
    }, 10);
  }

  _fade(fadeOut, fadeIn, callback) {
    let opacity = 50;
    let interval = setInterval(() => {
      if (opacity > 0) fadeOut.style.opacity = --opacity / 50;
      else if (opacity == 0 && fadeIn) {fadeOut.style.display = 'none'; fadeIn.style.display = ''; --opacity}
      else if (opacity > -50 && fadeIn) fadeIn.style.opacity = Math.abs(--opacity) / 50;
      else {
        clearInterval(interval);
        if (callback && typeof callback == 'function') callback();
      }
    }, 10);
  }


  answer_click() {
    this.yes.addEventListener('click', () => this._fade(this.answer, this.dropArea, this._standat));
  }

  _standat() {
    document.querySelector('body').style.display = '';
    [].slice.call(document.querySelector('#datalist').children).forEach(el => el.remove());
    document.querySelector('#dropArea [type="file"]').value = '';
  }


  dropArea_drop() {
    let self = this;

    function preventDefaults(event) {event.preventDefault(); event.stopPropagation();}
    function highlight(event) {self.dropArea.classList.add('highlight')}
    function unhighlight(event) {self.dropArea.classList.remove('highlight')}

    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => this.dropArea.addEventListener(eventName, preventDefaults, false));
    ['dragenter', 'dragover'].forEach(eventName => this.dropArea.addEventListener(eventName, highlight, false));
    ['dragleave', 'drop'].forEach(eventName => this.dropArea.addEventListener(eventName, unhighlight, false));

    this.dropArea.addEventListener('drop', e => this._readFile(e.dataTransfer.files), false);
  }


  sheetList_chage() {
    this.sheetList.addEventListener('input', event => {
      if (event.type != 'input') return;

      let page = parseInt(event.target.value);
      let data = XLSX.utils.sheet_to_json(this.book.Sheets[this.bookSheets[page]]);

      this._fade(this.counter, this.myChart, () => {
        document.querySelector('body').style.display = 'block';
        this._drawChart(data);
      });
    });
  }


  _drawChart(data) {
    let names = {};
    let series = [];
    let colors = {
      'Dll Handling Section':     '#f91807',
      'File System Section':      '#bb396c',
      'Registry Section':         '#ab47bc',
      'Process Section':          '#540017',
      'System info Section':      '#009f91',
      'Winsocket  Section':       '#5c6bc0',
      'Resource Section':         '#040054',
      'Thread Section':           '#868686',
      'Synchronization Section':  '#a77800',
      'Memory manage Section':    '#00b30c',
      'Fiber Section':            '#a5cf00',
      'Other functions':          '#000000',
    };

    data.forEach(json => {
      let sectName = json['Section name'];
      let funcName = json['Function name'];
      let value = json['Amount of calls'];

      if (!names[sectName]) {
        names[sectName] = Object.keys(names).length;
        series.push({
          text:sectName,
          style: {
            backgroundColor: colors[sectName],
            tooltip: {
              backgroundColor: colors[sectName],
            }
          },
          children: [{
            text: funcName,
            value: value,
            style: {
              backgroundColor: colors[sectName],
              tooltip: {
                backgroundColor: colors[sectName],
              }
            },
          }],
        });
      }
      else {
        series[names[sectName]].children.push({
          text: funcName,
          value: value,
          style: {
            backgroundColor: colors[sectName],
            tooltip: {
              backgroundColor: colors[sectName],
            }
          },
        });
      }
    });

    let myConfig = {
      series: series,
      "type":"treemap",
      "options": {
        "split-type":"balanced",
        "color-type":"palette",
        "palette":[
          // '#ff1100', '#009f91', '#a77800',
          // '#f4478a', '#5c6bc0', '#00b30c',
          // '#ab47bc', '#040054', '#a5cf00',
          // '#540017', '#000000', '#868686',
        ],
      },
    }

    zingchart.render({
      id: 'myChart',
      data: myConfig,
      height: '100%',
      width: '100%',
    });

    document.getElementById('treemap-layout').addEventListener('change', function(e) {
      myConfig.options['split-type'] = e.srcElement.value;
      zingchart.exec('myChart', 'setdata', {data:myConfig});
    });
  }

}



var main = new Main();
