'use strict';

const {app, BrowserWindow, Menu, ipcMain} = require('electron');
const {readdir} = require('fs');
const async = require('async');
const path = require('path');
const url = require('url');

const Config = require('./config.js');
const WindowMenu = require('./windowMenu.js');


class App {
  constructor() {
    this.window = {};

    this.config = new Config();
    this.windowMenu = new WindowMenu(app, this.window);

    process.env.NODE_ENV = this.config.mode;

    app.on('ready', () => this.ready());
    ipcMain.on('clear', () => this.ipcMain());
  }


  ready() {
    Menu.setApplicationMenu(Menu.buildFromTemplate(this.windowMenu.createMenu()));

    this.window.main = new BrowserWindow(this.config.window.main);
    this.window.main.on('closed', () => app.quit());

    this.window.main.loadURL(url.format({
      pathname: path.join(__dirname, 'pages', 'main.html'),
      protocol:'file:',
      slashes: true,
    }));
  }


}



module.exports = App;
