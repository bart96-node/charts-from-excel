class WindowMenu {
  constructor(app, window) {
    this.main = [
      {
        label:'File',
        submenu: [
          {
            label:'New',
            click: () => this.newChart(window),
            accelerator: process.platform == 'darwin' ? 'Command+N' : 'Ctrl+N',
          }, {
            label:'Clear',
            click: () => this.clear(window),
            accelerator: process.platform == 'darwin' ? 'Command+R' : 'Ctrl+R',
          }, {
            label:'Quit',
            click: () => this.quit(app),
            accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
          },
        ]
      },

      {
        label:'About',
        submenu: [
		  {
			label:'Author: Ульяна Кобзарь',
			enabled: false,
		  }, {
			label:'Version: v2.0.0',
			enabled: false,
		  },
		]
      }

    ];

  }


  createMenu() {
    if (process.platform == 'darwin') mainMenuTemplate.unshift({});
    if (process.env.NODE_ENV !== 'production') this.main.push({
      label:'Developer Tools',
      submenu: [
        // {
        //   role:'reload'
        // },
        {
          label:'Toggle DevTools',
          click: this.toggleDevTools,
          accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
        }
      ]
    });

    return this.main;
  }


  newChart(window) {window.main.reload();}
  clear(window) {window.main.webContents.send('clear');}
  quit(app) {app.quit();}

  toggleDevTools(item, focusedWindow) {focusedWindow.toggleDevTools();}

}



module.exports = WindowMenu;
